﻿///

$(() => {

    var logger = new Logger();
    var fileIdx = 0;
    var images: HTMLImageElement[] = [];
    var $img1 = $('#imgOneCanvas');
    var $img2 = $('#imgTwoCanvas');
    var $imgBlend = $('#imgBlendCanvas');

    $('#play').click(() => play());
   
    var w = 300;
    var h = 300;

    function setFile(f: any) {
        var idx = fileIdx;
        fileIdx = (fileIdx + 1) % 2;
        getFileData(f, (data) => {
            loadImage(data, img => {
                images[idx] = img;
                update();
            });
        });
    }

    function update() {
        if (!images[0] || !images[1]) {
            return;
        }

        // Could change w/h here...

        var $images = $img1.add($img2).add($imgBlend);
        $images.width(w).height(h).attr({ width: w, height: h });
        
        var ctx1 = getCanvas($img1);
        var ctx2 = getCanvas($img2);
        
        ctx1.drawImage(images[0], 0, 0);
        ctx2.drawImage(images[1], 0, 0);
    }

    function blendImage(pct: number) {
        var ctx1 = getCanvas($img1);
        var ctx2 = getCanvas($img2);
        var ctxBlend = getCanvas($imgBlend);
        var data1 = ctx1.getImageData(0, 0, w, h).data;
        var data2 = ctx2.getImageData(0, 0, w, h).data;
        var imgDataBlend = ctxBlend.getImageData(0, 0, w, h);
        var dataBlend = imgDataBlend.data;

        for (var i = 0; i < data1.length; i++) {
            var diff = data2[i] - data1[i];
            dataBlend[i] = Math.round(data1[i] + diff * pct);
        }

        ctxBlend.putImageData(imgDataBlend, 0, 0);
    }

    function getCanvas(j: JQuery) {
        return (<HTMLCanvasElement>j[0]).getContext('2d');
    }

    function loadImage(data: string, go: (img: HTMLImageElement) => void) {
        var img = new Image();
        img.onload = function() {
            go(img);
        };
        img.src = data;
    }

    var animation = $('<div>');

    function play() {
        var blendDurationMs = $('#blendDuration').val() * 1000;
        animation.height(0).animate({ height: 100 }, {
            duration: blendDurationMs, progress: function (a, pct) {
                blendImage(pct);
            }
        });
    }

    function handleFileSelect(evt: any) {
        evt.stopPropagation();
        evt.preventDefault();
        $('#drop_zone').removeClass('dragging');
        logger.clear();
        var files = evt.dataTransfer.files; // FileList object.

        if (files.length === 2) {
            setFile(files[0]);
            setFile(files[1]);
        } else if (files.length === 1) {
            setFile(files[0]);
        } else {
            logger.logFailure('unexpected number of files ' + files.length);
        }
    }



    function getFileData(f: any, go: (imgStr: string) => void) {
        var r = new FileReader();
        r.onload = function (e: any) {
            var contents = e.target.result;
            go(contents);
        }
        r.readAsDataURL(f);
    }

    function handleDragOver(evt: any) {
        evt.stopPropagation();
        evt.preventDefault();
        $('#drop_zone').addClass('dragging');
        evt.dataTransfer.dropEffect = 'copy';
    }

    function handleDragLeave(evt: any) {
        evt.stopPropagation();
        evt.preventDefault();
        $('#drop_zone').removeClass('dragging');
    }
    
    var dropZone = document.getElementById('drop_zone');
    dropZone.addEventListener('dragover', handleDragOver, false);
    dropZone.addEventListener('dragleave', handleDragLeave, false);
    dropZone.addEventListener('drop', handleFileSelect, false);
});