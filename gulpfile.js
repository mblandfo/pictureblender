var gulp = require('gulp');
var uglify = require('gulp-uglify');
var gzip = require('gulp-gzip');
var ts = require('gulp-typescript');
//var sourcemaps = require('gulp-sourcemaps');
 
var dest = './public';
 
gulp.task('js', function() {
	gulp.src(['src/**/*.ts', 'lib/**/*.ts'])
		//		.pipe(sourcemaps.init())	
		.pipe(ts({
			noImplicitAny: true,
			out: 'imageThing.js'
		}))
		//		.pipe(uglify())
		//		.pipe(sourcemaps.write())
		.pipe(gulp.dest(dest));
//		.pipe(gzip())
//		.pipe(gulp.dest(dest));
});

gulp.task('cssAndHtml', function() {
	gulp.src(['normalize.css', 'main.css', 'index.html'])
		.pipe(gulp.dest(dest));
		// .pipe(gzip())
		// .pipe(gulp.dest(dest));
});

gulp.task('default', ['js', 'cssAndHtml']);
